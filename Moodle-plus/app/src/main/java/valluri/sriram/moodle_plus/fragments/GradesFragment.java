package valluri.sriram.moodle_plus.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.adapters.GradesListAdapter;
import valluri.sriram.moodle_plus.items.GradeItem;
import valluri.sriram.moodle_plus.items.StringRequest;


/**
 * A simple {@link Fragment} subclass.
 */
public class GradesFragment extends Fragment {
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArrayCourses , jsonArrayGrades;
    private GradesListAdapter gradesListAdapter;
    private String[] gradeCourseName, gradeAssignment, gradeScore, gradeWeightage, gradeAbsoluteMarks;
    private ListView mListView;
    private View mView;
    private double score, weightage, out_of, absolute;
    private TextView mTextView;

    public GradesFragment() {
        // Required empty public constructor
    }
    public static GradesFragment newInstance() {
        return new GradesFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_grades, container, false);
        InitializeArrays();
        getGrades();
        return mView;
    }
    
    /**
      * Initialize arrays
      */
    private void InitializeArrays() {
        gradeCourseName = new String[0];
        gradeAssignment = new String[0];
        gradeScore = new String[0];
        gradeWeightage = new String[0];
        gradeAbsoluteMarks = new String[0];
    }
    
    /**
      * Method to raise dialog
      */
    private void RaiseDialog(String Title,String Message) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(Title);
            alertDialog.setMessage(Message);
            alertDialog.setNegativeButton("close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OnClicking it closes the dialog
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }catch(Exception e) {
            Log.d("TAG", "Show Dialog: " + e.getMessage());
        }
    }
    
    /**
      * Method to get Grades of corresponding courses
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object to get details
      */
    private void getGrades() {
        String url = getString(R.string.IP_VALUE_PORT)+"/default/grades.json";
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArrayCourses = responseJSON.getJSONArray("courses");
                    jsonArrayGrades = responseJSON.getJSONArray("grades");
                    int k = jsonArrayCourses.length();
                    gradeCourseName = new String[k];
                    gradeAssignment = new String[k];
                    gradeScore = new String[k];
                    gradeWeightage = new String[k];
                    gradeAbsoluteMarks = new String[k];
                    for (int i = 0; i < k ; i++) {
                        JSONObject jsonObjectCourses = jsonArrayCourses.getJSONObject(i);
                        JSONObject jsonObjectGrades = jsonArrayGrades.getJSONObject(i);

                        score = jsonObjectGrades.getDouble("score");
                        out_of = jsonObjectGrades.getDouble("out_of");
                        weightage = jsonObjectGrades.getDouble("weightage");
                        absolute = ((score / out_of) * (weightage));
                        absolute = Math.round(absolute * 100.0) / 100.0;
                        gradeCourseName[i] = jsonObjectCourses.getString("code").toUpperCase();
                        gradeAssignment[i] = jsonObjectGrades.getString("name");
                        gradeScore[i] = String.valueOf(score)+"/"+String.valueOf(out_of);
                        gradeWeightage[i] = String.valueOf(weightage);
                        gradeAbsoluteMarks[i] = String.valueOf(absolute);
                    }
                    makeGrades(mView);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get Grades", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }
    
    /**
      * Method to make Grades details in type view 
      * 
      */
    private void makeGrades(View view) {
        ArrayList<GradeItem> GradeItems = new ArrayList<GradeItem>();

        for (int i = 0; i < gradeCourseName.length; i++) {
            GradeItem items = new GradeItem(gradeCourseName[i], gradeAssignment[i], gradeScore[i], gradeWeightage[i], gradeAbsoluteMarks[i]);
            GradeItems.add(items);
        }

        // update gradesListAdapter with grade items
        gradesListAdapter = new GradesListAdapter(getActivity(), GradeItems);
        mListView = (ListView) view.findViewById(R.id.lv_fragment_grades);
        mTextView = (TextView) view.findViewById(R.id.tv_grades_empty);
        mListView.setAdapter(gradesListAdapter);
        if(gradeCourseName.length == 0){
            mTextView.setVisibility(View.VISIBLE);
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RaiseDialog(gradeCourseName[position] + " : " + gradeAssignment[position], "Score : " + gradeScore[position] + "\n" + "Weightage : " + gradeWeightage[position] + "\n" + "Absolute Marks : " + gradeAbsoluteMarks[position]);
            }
        });
    }

}
