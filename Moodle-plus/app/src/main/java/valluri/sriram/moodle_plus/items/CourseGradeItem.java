package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class CourseGradeItem {

    private String mCourseGradeAssignment, mCourseGradeScore, mCourseGradeWeightage, mCourseGradeAbsoluteMarks;

    public CourseGradeItem(String CourseGradeAssignment, String CourseGradeScore, String CourseGradeWeightage, String CourseGradeAbsoluteMarks) {
        this.mCourseGradeAssignment = CourseGradeAssignment;
        this.mCourseGradeScore = CourseGradeScore;
        this.mCourseGradeWeightage = CourseGradeWeightage;
        this.mCourseGradeAbsoluteMarks = CourseGradeAbsoluteMarks;

    }

    public String getCourseGradeAssignment() {
        return mCourseGradeAssignment;
    }

    public void setCourseGradeAssignment(String CourseGradeAssignment) {
        this.mCourseGradeAssignment = CourseGradeAssignment;
    }

    public String getCourseGradeScore() {
        return mCourseGradeScore;
    }

    public void setCourseGradeScore(String CourseGradeScore) {
        this.mCourseGradeScore = CourseGradeScore;
    }

    public String getCourseGradeWeightage() {
        return mCourseGradeWeightage;
    }

    public void setCourseGradeWeightage(String CourseGradeWeightage) {
        this.mCourseGradeWeightage = CourseGradeWeightage;
    }

    public String getCourseGradeAbsoluteMarks() {
        return mCourseGradeAbsoluteMarks;
    }

    public void setCourseGradeAbsoluteMarks(String CourseGradeAbsoluteMarks) {
        this.mCourseGradeAbsoluteMarks = CourseGradeAbsoluteMarks;
    }
}
