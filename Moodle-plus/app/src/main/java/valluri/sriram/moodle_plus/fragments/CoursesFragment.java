package valluri.sriram.moodle_plus.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.activities.CourseActivity;
import valluri.sriram.moodle_plus.adapters.CoursesListAdapter;
import valluri.sriram.moodle_plus.items.CourseItem;
import valluri.sriram.moodle_plus.items.StringRequest;


/**
 * A simple {@link Fragment} subclass.
 */
public class CoursesFragment extends Fragment{

    private CoursesListAdapter coursesListAdapter;
    private String[] courseNameArray, courseDescArray, courseCodeArray, courseCreditsArray;
    private ListView mListView;
    private View mView;
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArray;
    private TextView mTextView;

    public static CoursesFragment newInstance() {
        return new CoursesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        super.onActivityCreated(savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_courses, container, false);
        InitializeArrays();
        getCourses();
        return mView;

    }

    /**
      * Initialize arrays
      */
    private void InitializeArrays() {
        courseNameArray = new String[0];
        courseDescArray = new String[0];
        courseCodeArray = new String[0];
        courseCreditsArray = new String[0];
    }

    /**
      * Method to get courses and it details
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object to get details
      */
    private void getCourses() {
        String url = getString(R.string.IP_VALUE_PORT)+"/courses/list.json";
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArray = responseJSON.getJSONArray("courses");
                    courseNameArray = new String[jsonArray.length()];
                    courseDescArray = new String[jsonArray.length()];
                    courseCodeArray = new String[jsonArray.length()];
                    courseCreditsArray = new String[jsonArray.length()];

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        courseNameArray[i] = jsonObject.getString("name");
                        courseDescArray[i] = jsonObject.getString("description");
                        courseCodeArray[i] = jsonObject.getString("code").toUpperCase();
                        courseCreditsArray[i] = jsonObject.getString("l_t_p");
                    }
                    makeCourses(mView);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get courses", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    /**
      * Method to make courses details in type view 
      * 
      */
    private void makeCourses(View view) {
        ArrayList<CourseItem> courseItems = new ArrayList<CourseItem>();

        for (int i = 0; i < courseNameArray.length; i++) {
            CourseItem items = new CourseItem(courseNameArray[i], courseDescArray[i], courseCodeArray[i], courseCreditsArray[i]);
            courseItems.add(items);
        }
        
        // update courseListAdapter with course items
        coursesListAdapter = new CoursesListAdapter(getActivity(), courseItems);
        mListView = (ListView) view.findViewById(R.id.lv_fragment_courses);
        mTextView = (TextView) view.findViewById(R.id.tv_courses_empty);
        mListView.setAdapter(coursesListAdapter);
        if(courseNameArray.length == 0){
            mTextView.setVisibility(View.VISIBLE);
        }
        mListView.setLongClickable(true);
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                RaiseDialog(courseNameArray[position], courseDescArray[position] + "\n\n" +
                        "Course Code : " + courseCodeArray[position] + "\n\n" +
                        "Course Credits : " + courseCreditsArray[position]);
                return true;
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), CourseActivity.class);
                i.putExtra("CourseName", courseNameArray[position]);
                i.putExtra("CourseCode", courseCodeArray[position]);
                startActivity(i);
            }
        });
    }
    
    /**
      * Method to raise dialog
      */
    private void RaiseDialog(String Title, String Message) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(Title);
            alertDialog.setMessage(Message);
            alertDialog.setCancelable(false);
            alertDialog.setNegativeButton("close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OnClicking it closes the dialog
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }catch(Exception e) {
            Log.d("TAG", "Show Dialog: " + e.getMessage());
        }
    }
    
}
