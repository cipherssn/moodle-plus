package valluri.sriram.moodle_plus.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.adapters.CommentsListAdapter;
import valluri.sriram.moodle_plus.items.CommentItem;
import valluri.sriram.moodle_plus.items.StringRequest;

public class CommentActivity extends AppCompatActivity {

    private String mThreadNumber;
    private Toolbar mToolbar;
    private CommentsListAdapter commentsListAdapter;
    private String[] commentDescArray, commentAuthorArray, commentTimestampArray;
    private ListView mListView;
    private com.android.volley.RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArrayUsers;
    private TextView mTextView;
    private String mCourseCode;
    private JSONArray jsonArrayComments;
    private FloatingActionButton mNewCommentBtn;
    private EditText mEditText;
    private String mDescriptionStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        bindViews();
        if (getIntent().getExtras() != null) {
            String mTitle = getIntent().getStringExtra("ThreadTitle");
            mThreadNumber = getIntent().getStringExtra("ThreadNumber");
            setupToolbar(mTitle);
        }
        InitializeArrays();
        getComments();
        mNewCommentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(CommentActivity.this);
                dialog.setContentView(R.layout.dialog_activity_comment);
                dialog.setTitle("New Comment");
                dialog.setCancelable(false);
                dialog.show();
                Button Closebtn = (Button) dialog.findViewById(R.id.btn_comment_dialog_close);
                Button Postbtn = (Button) dialog.findViewById(R.id.btn_dialog_new_comment);
                Closebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Postbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEditText = (EditText) dialog.findViewById(R.id.newcomment);
                        mDescriptionStr = mEditText.getText().toString();
                        addNewComment();
                        dialog.dismiss();
                    }
                });
            }
        });
    }
    
    /**
      * Method is to add new comment
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      */
    private void addNewComment() {
        String url =getString(R.string.IP_VALUE_PORT)+"/threads/post_comment.json?thread_id="+mThreadNumber+"&description="+mDescriptionStr;
        Log.i("Posting Comment", url);

        final ProgressDialog processDialog = new ProgressDialog(this);
        processDialog.setMessage("Posting Comment");
        processDialog.show();

        requestQueue = Volley.newRequestQueue(this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this, Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    boolean success= responseJSON.getBoolean("success");
                    afterGettingResponse(success);
                    processDialog.hide();
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                processDialog.hide();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    /**
      * This method is check whether comment posted or not after getting response
      *
      */
    private void afterGettingResponse(boolean success) {
        if(success){
            finish();
            startActivity(getIntent());
        }else {
            Toast.makeText(getApplicationContext(),"Comment not posted", Toast.LENGTH_LONG).show();
        }
    }
    
    /**
      * Method to bind xml views
      */
    private void bindViews() {
        mListView = (ListView)findViewById(R.id.lv_activity_comment);
        mToolbar = (Toolbar) findViewById(R.id.tb_activity_comment);
        mNewCommentBtn = (FloatingActionButton)findViewById(R.id.fab_activity_comment);
        mTextView = (TextView)findViewById(R.id.tv_comments_empty);
    }
    
    /**
      * Method to setup toolbar
      */
    private void setupToolbar(String Title) {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }

    /**
      * Method to initialize arrays
      */
    private void InitializeArrays() {
        commentDescArray = new String[0];
        commentAuthorArray = new String[0];
        commentTimestampArray = new String[0];
    }

    /**
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      */
    private void getComments() {
        //
        String url = getString(R.string.IP_VALUE_PORT)+"/threads/thread.json/"+mThreadNumber;

        Log.i("Server Connecting", url);
        requestQueue = Volley.newRequestQueue(this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this, Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArrayComments = responseJSON.getJSONArray("comments");
                    jsonArrayUsers = responseJSON.getJSONArray("comment_users");
                    int k = jsonArrayComments.length();
                    commentDescArray = new String[k];
                    commentAuthorArray = new String[k];
                    commentTimestampArray = new String[k];
                    for (int i = 0; i < k ; i++) {
                        JSONObject jsonObject = jsonArrayComments.getJSONObject(i);
                        commentDescArray[k-i-1] = jsonObject.getString("description");
                        commentAuthorArray[k-i-1] = jsonArrayUsers.getJSONObject(i).getString("first_name")+" "+jsonArrayUsers.getJSONObject(i).getString("last_name");
                        commentTimestampArray[k-i-1] = jsonObject.getString("created_at");

                    }
                    makeComments();
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Fail to get Comments", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    /**
      * Method to make comments
      */
    private void makeComments() {
        // ArrayList of commentitem
        ArrayList<CommentItem> CommentItems = new ArrayList<CommentItem>();

        for (int i = 0; i < commentDescArray.length; i++) {
            CommentItem items = new CommentItem(commentDescArray[i], commentAuthorArray[i], commentTimestampArray[i]);
            CommentItems.add(items);
        }

        CommentsListAdapter CommentsListAdapter = new CommentsListAdapter(getApplicationContext(), CommentItems);
        mListView.setAdapter(CommentsListAdapter);
        if(commentDescArray.length==0){
            mTextView.setVisibility(View.VISIBLE);
        }
    }

}
