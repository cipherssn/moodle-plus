package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.CourseGradeItem;

public class CourseGradesListAdapter extends BaseAdapter{

    Context context;
    List<CourseGradeItem> CourseGradeItem;
    private TextView mCourseGradeAssignment, mCourseGradeScore, mCourseGradeWeightage, mCourseGradeAbsoluteMarks;

    public CourseGradesListAdapter(Context context, List<CourseGradeItem> CourseGradeItem) {
        this.context = context;
        this.CourseGradeItem = CourseGradeItem;
    }

    @Override
    public int getCount() {
        return CourseGradeItem.size();
    }

    @Override
    public Object getItem(int position) {
        return CourseGradeItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return CourseGradeItem.indexOf(getItem(position));
    }

    @Override
    /**
     * Method to get View which gives details of the particular course assignment/minor
     *
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.fragment_course_grades_item, null);
        }

        mCourseGradeAssignment = (TextView) convertView.findViewById(R.id.tv_course_grades_assignment);
        mCourseGradeScore = (TextView) convertView.findViewById(R.id.tv_course_grades_score);
        mCourseGradeWeightage = (TextView) convertView.findViewById(R.id.tv_course_grades_weightage);
        mCourseGradeAbsoluteMarks = (TextView) convertView.findViewById(R.id.tv_course_grades_absolute_marks);

        CourseGradeItem CourseGradeitem = CourseGradeItem.get(position);
        // setting the image resource and title
        mCourseGradeAssignment.setText(CourseGradeitem.getCourseGradeAssignment());
        mCourseGradeScore.setText(CourseGradeitem.getCourseGradeScore());
        mCourseGradeWeightage.setText(CourseGradeitem.getCourseGradeWeightage());
        mCourseGradeAbsoluteMarks.setText(CourseGradeitem.getCourseGradeAbsoluteMarks());
        return convertView;
    }

}
