package valluri.sriram.moodle_plus.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.StringRequest;

public class LoginActivity extends AppCompatActivity {

    private EditText mUserName, mPassword;
    public View focusView;
    private String mUserNameStr, mPasswordStr;
    private RequestQueue requestQueue;
    private Boolean mLogin;
    private Button mLoginBtn;
    private String IpAddress ;
    private String Port="8000";
    private JSONObject responseJSON;
    public static final String PREFS="MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Checks if the app is connected to internet or not      
        if(isOnline()) {
            //Restore preferences
            SharedPreferences sharedPreferences=getSharedPreferences(PREFS, Context.MODE_PRIVATE);
            String cookie=sharedPreferences.getString(StringRequest.SESSION_COOKIE, null);
            if (cookie!=null) {
                LaunchHomeActivity(true);
            }
            bindViews();
            mLoginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDetails();
                    if (nameError(mUserName)) {
                        if ( passError(mPassword)) {
                            ConnectingServer();
                        }
                    }
                }
            });
        }else {
            RaiseDialog("Device Offline", "Please check your connection", android.R.drawable.ic_dialog_alert, true, true);
        }
    }

    /**
      * Checks the internet connection of the user
      */
    private boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    /**
      * Rasises a alert dialog if the user is offline
      */
    private void RaiseDialog(String Title, String Message, int IconId, Boolean Close, Boolean Settings) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
            alertDialog.setTitle(Title);
            alertDialog.setMessage(Message);
            alertDialog.setIcon(IconId);
            if(Close) {
                alertDialog.setNegativeButton("close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // OnClicking it closes the dialog
                        dialog.cancel();
                    }
                });
            }
            if(Settings) {
                alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // OnClicking it it goes to Settings
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        dialog.cancel();
                    }
                });
            }
            alertDialog.show();
        } catch(Exception e) {
            Log.d("TAG", "Show Dialog: " + e.getMessage());
        }
    }

    /**
      * Method to bind xml views
      */
    private void bindViews() {
        mUserName = (EditText)findViewById(R.id.userName);
        mPassword = (EditText)findViewById(R.id.password);
        mLoginBtn = (Button)findViewById(R.id.button);
    }
    
    /**
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      */
    private void ConnectingServer() {
        mLogin = false;
        String url = getString(R.string.IP_VALUE_PORT)+"/default/login.json?userid="+mUserNameStr+"&password="+mPasswordStr;
        Log.i("Server Connecting", url);

        final ProgressDialog processDialog = new ProgressDialog(this);
        processDialog.setMessage("Connecting to the server");
        processDialog.show();

        requestQueue = Volley.newRequestQueue(this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this, Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    mLogin = responseJSON.getBoolean("success");
                    Log.e("login", mLogin.toString());
                    processDialog.hide();
                    LaunchHomeActivity(mLogin);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                processDialog.hide();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }
     
    /**
      * Getting detais of username and password
      */
    private void getDetails() {
        mUserNameStr  = mUserName.getText().toString();
        mPasswordStr  = mPassword.getText().toString();
    }

    /**
      * Error details of username
      */
    private boolean nameError(EditText name) {
        if ((name == null) || (name.length() < 1)) {
            name.setError(getString(R.string.empty_error_message));
            focusView = name;
            focusView.requestFocus();
            return false;
        }else if ((name.getText().toString().startsWith(" "))) {
            name.setError(getString(R.string.not_allowed_error_message));
            focusView = name;
            focusView.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    /**
      * Error details of password
      */
    private boolean passError(EditText name) {
        if ((name == null) || (name.length() < 1)) {
            name.setError(getString(R.string.empty_error_message));
            focusView = name;
            focusView.requestFocus();
            return false;
        }else {
            return true;
        }
    }

    /**
      * Method to launch home activity
      */
    private void LaunchHomeActivity(Boolean login){
        if(login) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else {
            RaiseDialog("Invalid Login", "Entered Information is not valid", android.R.drawable.ic_dialog_alert, true, false);
        }
    }

}
