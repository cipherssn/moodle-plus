package valluri.sriram.moodle_plus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.adapters.CourseTabsPageAdapter;
import valluri.sriram.moodle_plus.items.StringRequest;

public class CourseActivity extends AppCompatActivity {
    private static final String POSITION = "POSITION";
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        if (getIntent().getExtras() != null) {
            String mTitle = getIntent().getStringExtra("CourseCode");
            setupToolbar(mTitle);
        }
        setupTabsWithPager();
    }
    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(POSITION, mTabLayout.getSelectedTabPosition());
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mViewPager.setCurrentItem(savedInstanceState.getInt(POSITION));
    }
    
    /**
      * Method to setup the toolbar 
      */
    private void setupToolbar(String title) {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_activity_course);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }
    
    /**
      * Method to setup the tabs with Pager 
      */
    private void setupTabsWithPager() {
        mTabLayout = (TabLayout) findViewById(R.id.tl_activity_course);
        mViewPager = (ViewPager) findViewById(R.id.pager_course);
        mViewPager.setAdapter(new CourseTabsPageAdapter(getSupportFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
    }

}
