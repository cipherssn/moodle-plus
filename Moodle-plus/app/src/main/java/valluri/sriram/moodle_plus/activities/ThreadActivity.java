package valluri.sriram.moodle_plus.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.StringRequest;

public class ThreadActivity extends AppCompatActivity {

    private EditText mTitle,mDescription;
    private Button mSubmitBtn;
    private JSONObject responseJSON;
    private RequestQueue requestQueue;
    String title,description,courseCode,url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        bindViews();
        setupToolbar();
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDetails();
                onSubmit();
            }
        });
    }
    
    /**
      * Method to setup the toolbar 
      */
    private void setupToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_activity_thread);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        getSupportActionBar().setTitle("New Thread");
    }
     
    /**
      * Method to bind xml views
      */
    private void bindViews() {
        mTitle = (EditText)findViewById(R.id.title);
        mDescription = (EditText)findViewById(R.id.description);
        mSubmitBtn = (Button)findViewById(R.id.button);
    }
    
    /**
      * Getting detais of title, description, courseCode
      */
    private void getDetails() {
        title = mTitle.getText().toString();
        description = mDescription.getText().toString();
        courseCode = getIntent().getExtras().getString("CourseCode");
    }
    
    /**
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      */
    private void onSubmit() {
        url =  getString(R.string.IP_VALUE_PORT)+"/threads/new.json?title="+title+"&description="+description+"&course_code="+courseCode.toLowerCase() ;
        Log.i("Posting thread", url);
        
        //Setting processing dialogue
        final ProgressDialog processDialog = new ProgressDialog(this);
        processDialog.setMessage("Posting Thread");
        processDialog.show();

        requestQueue = Volley.newRequestQueue(this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this, Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    int thread_id = responseJSON.getInt("thread_id");
                    boolean success = responseJSON.getBoolean("success");
                    afterGettingResponse(thread_id,success);
                    processDialog.hide();
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                processDialog.hide();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    /**
      * Method to show thread posted or not after getting response
      */
    private void afterGettingResponse(int thread_id, boolean success ) {
        if(success) {
            Toast.makeText(getApplicationContext(), "Thread got posted", Toast.LENGTH_LONG).show();
            // go to course page
            Intent intent = new Intent(this, CourseActivity.class);
            startActivity(intent);

        }else {
            Toast.makeText(getApplicationContext(), "Thread did not get posted", Toast.LENGTH_LONG).show();
        }
    }

}
