package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.NotificationItem;

public class NotificationsListAdapter extends BaseAdapter{
    Context context;
    List<NotificationItem> NotificationItem;
    private TextView mNotificationCourseName, mNotificationAuthor, mNotificationTimeStamp;

    /**
      * Constructor method
      *
      */ 
    public NotificationsListAdapter(Context context, List<NotificationItem> NotificationItem) {
        this.context = context;
        this.NotificationItem = NotificationItem;
    }

    @Override
    public int getCount() {
        return NotificationItem.size();
    }

    @Override
    public Object getItem(int position) {
        return NotificationItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return NotificationItem.indexOf(getItem(position));
    }

    @Override
    /**
      * Method to get View which gives information about notification
      *
      */ 
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.fragment_notifications_item, null);
        }

        mNotificationCourseName = (TextView) convertView.findViewById(R.id.tv_notification_course_name);
        mNotificationAuthor = (TextView) convertView.findViewById(R.id.tv_notification_author);
        mNotificationTimeStamp = (TextView) convertView.findViewById(R.id.tv_notifications_time_Stamp);

        NotificationItem courseitem = NotificationItem.get(position);

        String startTime = courseitem.getNotificationTimeStamp();
        StringTokenizer tk = new StringTokenizer(startTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateParsed = null;
        Date dt = null;
        try {
            dateParsed = (Date)formatter.parse(date);
            dt = sdf.parse(time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // setting the image resource and title
        mNotificationCourseName.setText(courseitem.getNotificationCourseName());
        mNotificationAuthor.setText(courseitem.getNotificationAuthor());
        mNotificationTimeStamp.setText(dateParsed.toString().substring(0,10) +" "+sdfs.format(dt));
        return convertView;
    }

}
