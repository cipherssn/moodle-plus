package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class NotificationItem {

    private String mNotificationCourseName, mNotificationAuthor, mNotificationTimeStamp;

    public NotificationItem(String NotificationCourseName, String NotificationAuthor, String NotificationTimeStamp) {
        this.mNotificationCourseName = NotificationCourseName;
        this.mNotificationAuthor = NotificationAuthor;
        this.mNotificationTimeStamp = NotificationTimeStamp;
    }

    public String getNotificationCourseName() {
        return mNotificationCourseName;
    }

    public void setNotificationCourseName(String NotificationCourseName) {
        this.mNotificationCourseName = NotificationCourseName;
    }

    public String getNotificationAuthor() {
        return mNotificationAuthor;
    }

    public void setNotificationAuthor(String NotificationAuthor) {
        this.mNotificationAuthor = NotificationAuthor;
    }

    public String getNotificationTimeStamp() {
        return mNotificationTimeStamp;
    }

    public void setNotificationTimeStamp(String NotificationTimeStamp) {
        this.mNotificationTimeStamp = NotificationTimeStamp;
    }
}
