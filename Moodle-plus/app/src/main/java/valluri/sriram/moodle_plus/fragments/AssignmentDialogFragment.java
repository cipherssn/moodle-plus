package valluri.sriram.moodle_plus.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.StringRequest;

/**
 * Created by lens on 24/02/16.
 */
public class AssignmentDialogFragment extends DialogFragment {

    private View rootView;
    private TextView mDialogTitle, mDialogDescription , mDialogCreatedAt, mDialogDeadLine, mDialogTimeRemaining, mDialogLateDaysAllowed, mDialogPreviousSubmission;
    private String AssignmentNumber;
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private Button mCloseBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setContentView(R.layout.dialog_fragment_assignment);
        rootView = inflater.inflate(R.layout.dialog_fragment_assignment, container, false);
        Bundle mArgs = getArguments();
        AssignmentNumber = mArgs.getString("AssignmentNumber");
        bindViews();
        RaiseDialog();
        mCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        return rootView;
    }
    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    /**
      * Method to raise Dialog for assignment details
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object to get details
      */
    private void RaiseDialog() {
        String url=getString(R.string.IP_VALUE_PORT)+"/courses/assignment.json/"+AssignmentNumber;
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    JSONObject Info = responseJSON.getJSONObject("assignment");
                    JSONArray Submissions = responseJSON.getJSONArray("submissions");
                    mDialogTitle.setText(Info.getString("name"));
                    String startTime1 = Info.getString("created_at");
                    String startTime2 = Info.getString("deadline");

                    StringTokenizer tk1 = new StringTokenizer(startTime1);
                    StringTokenizer tk2 = new StringTokenizer(startTime2);

                    String date1 = tk1.nextToken();
                    String date2 = tk2.nextToken();
                    String time1 = tk1.nextToken();
                    String time2 = tk2.nextToken();

                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                    SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateParsed1 = null;
                    Date dt1 = null;
                    Date dateParsed2 = null;
                    Date dt2 = null;

                    final Calendar c = Calendar.getInstance();
                    String yy = String.valueOf(c.get(Calendar.YEAR));
                    String mm = String.valueOf(c.get(Calendar.MONTH));
                    String dd = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
                    String CurrentDate = yy+"-"+mm+"-"+dd;
                    Date Currentdate = null;
                    try {
                        dateParsed1 = (Date)formatter.parse(date1);
                        dt1 = sdf.parse(time1);
                        dateParsed2 = (Date)formatter.parse(date2);
                        dt2 = sdf.parse(time2);
                        Currentdate = (Date)formatter.parse(CurrentDate);
                    }catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    long diff = Currentdate.getTime() - dateParsed2.getTime();
                    System.out.print(Currentdate.getTime() +"-"+dateParsed2.getTime());
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    long days = hours / 24;
                    mDialogCreatedAt.setText(dateParsed1.toString().substring(0,10) +" "+sdfs.format(dt1));
                    mDialogDescription.setText(Html.fromHtml(Info.getString("description")).toString());
                    mDialogDeadLine.setText(dateParsed2.toString().substring(0,10) +" "+sdfs.format(dt2));
                    mDialogLateDaysAllowed.setText(String.valueOf(Info.getInt("late_days_allowed")));
                    mDialogPreviousSubmission.setText("");
                    mDialogTimeRemaining.setText(String.valueOf(days) + " Days");
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get Assignment Information", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }
    
    /**
     *  Method to bind xml views 
     *
     */
    private void bindViews() {
        mDialogTitle = (TextView) rootView.findViewById(R.id.tv_assignment_title);
        mDialogDescription = (TextView) rootView.findViewById(R.id.tv_assignment_description);
        mDialogCreatedAt = (TextView) rootView.findViewById(R.id.tv_created_at);
        mDialogDeadLine = (TextView) rootView.findViewById(R.id.tv_dead_line);
        mDialogTimeRemaining = (TextView) rootView.findViewById(R.id.tv_time_remaining);
        mDialogLateDaysAllowed = (TextView) rootView.findViewById(R.id.tv_late_days_allowed);
        mDialogPreviousSubmission = (TextView) rootView.findViewById(R.id.tv_previous_submission);
        mCloseBtn = (Button) rootView.findViewById(R.id.btn_dialog_close);
    }

}