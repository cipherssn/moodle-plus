package valluri.sriram.moodle_plus.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import valluri.sriram.moodle_plus.fragments.AssignmentsFragment;
import valluri.sriram.moodle_plus.fragments.CourseGradesFragment;
import valluri.sriram.moodle_plus.fragments.ThreadsFragment;

/**
 * Created by lens on 20/02/16.
 */

public class CourseTabsPageAdapter extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 3;
    private final String[] tabTitles = new String[]{"Threads", "Assignments", "Grades"};
    /**
     * Constructor Method
     *
     */
    public CourseTabsPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    /**
     * Method to get item that index represents
     *
     */
    public Fragment getItem(int index) {
        switch (index) {
            case 0:
                return ThreadsFragment.newInstance();
            case 1:
                return AssignmentsFragment.newInstance();
            case 2:
                return CourseGradesFragment.newInstance();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return PAGE_COUNT;
    }

}