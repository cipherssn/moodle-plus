package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class CourseItem {

    private String mCourseName, mCourseDescription, mCourseCode, mCourseCredits;

    public CourseItem(String CourseName, String CourseDescription, String CourseCode, String CourseCredits) {
        this.mCourseName = CourseName;
        this.mCourseDescription = CourseDescription;
        this.mCourseCode = CourseCode;
        this.mCourseCredits = CourseCredits;
    }

    public String getCourseName() {
        return mCourseName;
    }

    public void setCourseName(String CourseName) {
        this.mCourseName = CourseName;
    }

    public String getCourseDescription() {
        return mCourseDescription;
    }

    public void setCourseDescription(String CourseDescription) {
        this.mCourseDescription = CourseDescription;
    }

    public String getCourseCode() {
        return mCourseCode;
    }

    public void setCourseCode(String CourseCode) {
        this.mCourseCode = CourseCode;
    }

    public String getCourseCredits() {
        return mCourseCredits;
    }

    public void setCourseCredits(String CourseCredits) {
        this.mCourseCredits = CourseCredits;
    }
    }
