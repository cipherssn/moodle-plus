package valluri.sriram.moodle_plus.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.activities.CourseActivity;
import valluri.sriram.moodle_plus.adapters.AssignmentsListAdapter;
import valluri.sriram.moodle_plus.items.AssignmentItem;
import valluri.sriram.moodle_plus.items.StringRequest;

//import com.android.volley.toolbox.StringRequest;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AssignmentsFragment extends Fragment {

    private String[] AssignmentTitleArray, AssignmentBodyArray, AssignmentTimeStampArray;
    private int[] AssignmentID;
    private AssignmentsListAdapter AssignmentsListAdapter;
    private ListView mListView;
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArray;
    private View mview;
    private TextView mTextView;
    private String mCourseCode;

    public AssignmentsFragment() {
        // Required empty public constructor
    }

    public static AssignmentsFragment newInstance() {
        return new AssignmentsFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mview = inflater.inflate(R.layout.fragment_assignments, container, false);
        InitializeArrays();
        getAssignments();
        return mview;
    }

    /**
      * Method to initialize arrays
      *
      */
    private void InitializeArrays() {
        AssignmentTitleArray = new String[0];
        AssignmentBodyArray = new String[0];
        AssignmentTimeStampArray = new String[0];
        AssignmentID = new int[0];
    }
    
    /**
      * Method to get assignments of particular course
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      *
      */
    private void getAssignments() {
        CourseActivity activity = (CourseActivity) getActivity();
        mCourseCode = activity.getIntent().getExtras().getString("CourseCode");
        String url=getString(R.string.IP_VALUE_PORT)+"/courses/course.json/"+mCourseCode+"/assignments";
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArray = responseJSON.getJSONArray("assignments");
                    AssignmentTitleArray = new String[jsonArray.length()];
                    AssignmentBodyArray = new String[jsonArray.length()];
                    AssignmentTimeStampArray = new String[jsonArray.length()];
                    AssignmentID = new int[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AssignmentTimeStampArray[i] = jsonObject.getString("created_at");
                        AssignmentBodyArray[i] = StringAbstract(jsonObject.getString("name"), 1).substring(1);
                        AssignmentTitleArray[i] = StringAbstract(jsonObject.getString("name"), 0);
                        AssignmentID[i] = jsonObject.getInt("id");
                    }
                    MakeAssignment(mview);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get Assignments", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    private void MakeAssignment(View view) {
        ArrayList<AssignmentItem> AssignmentItems = new ArrayList<AssignmentItem>();

        for (int i = 0; i < AssignmentTitleArray.length; i++) {
            AssignmentItem items = new AssignmentItem(AssignmentTitleArray[i], AssignmentBodyArray[i], AssignmentTimeStampArray[i]);
            AssignmentItems.add(items);
        }

        AssignmentsListAdapter = new AssignmentsListAdapter(getActivity(), AssignmentItems);
        mListView = (ListView) view.findViewById(R.id.lv_fragment_assignments);
        mTextView = (TextView) view.findViewById(R.id.tv_assignments_empty);
        mListView.setAdapter(AssignmentsListAdapter);
        if(AssignmentTitleArray.length == 0) {
            mTextView.setVisibility(View.VISIBLE);
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle args = new Bundle();
                args.putString("AssignmentNumber", String.valueOf(AssignmentID[position]));
                FragmentManager fm = getFragmentManager();
                AssignmentDialogFragment dialogFragment = new AssignmentDialogFragment ();
                dialogFragment.setArguments(args);
                dialogFragment.show(fm, "Sample Fragment");
            }
        });
    }
     
    /**
      * Method to abstract required string 
      *
      */ 
    private String StringAbstract(String Description ,int position){
        return Description.split(":")[position];
    }

}
