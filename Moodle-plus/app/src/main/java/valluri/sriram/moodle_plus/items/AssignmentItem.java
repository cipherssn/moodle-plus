package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class AssignmentItem {

    private String mAssignmentTitle, mAssignmentBody, mAssignmentTimeStamp;

    public AssignmentItem(String AssignmentTitle, String AssignmentBody, String AssignmentTimeStamp) {
        this.mAssignmentTitle = AssignmentTitle;
        this.mAssignmentBody = AssignmentBody;
        this.mAssignmentTimeStamp = AssignmentTimeStamp;
    }

    public String getAssignmentTitle() {
        return mAssignmentTitle;
    }

    public void setAssignmentTitle(String AssignmentTitle) {
        this.mAssignmentTitle = AssignmentTitle;
    }

    public String getAssignmentBody() {
        return mAssignmentBody;
    }

    public void setAssignmentBody(String AssignmentBody) {
        this.mAssignmentBody = AssignmentBody;
    }

    public String getAssignmentTimeStamp() {
        return mAssignmentTimeStamp;
    }

    public void setAssignmentTimeStamp(String AssignmentTimeStamp) {
        this.mAssignmentTimeStamp = AssignmentTimeStamp;
    }
}
