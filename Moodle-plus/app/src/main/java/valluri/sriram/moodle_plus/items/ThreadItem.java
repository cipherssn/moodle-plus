package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class ThreadItem {

    private String mThreadTitle, mThreadDescription, mThreadAuthor, mThreadTimestamp;

    public ThreadItem(String ThreadTitle, String ThreadDescription, String ThreadAuthor, String ThreadTimestamp) {
        this.mThreadTitle = ThreadTitle;
        this.mThreadDescription = ThreadDescription;
        this.mThreadAuthor = ThreadAuthor;
        this.mThreadTimestamp = ThreadTimestamp;
    }

    public String getThreadTitle() {
        return mThreadTitle;
    }

    public void setThreadTitle(String ThreadTitle) {
        this.mThreadTitle = ThreadTitle;
    }

    public String getThreadDescription() {
        return mThreadDescription;
    }

    public void setThreadDescription(String ThreadDescription) {
        this.mThreadDescription = ThreadDescription;
    }

    public String getThreadAuthor() {
        return mThreadAuthor;
    }

    public void setThreadAuthor(String ThreadAuthor) {
        this.mThreadAuthor = ThreadAuthor;
    }

    public String getThreadTimestamp() {
        return mThreadTimestamp;
    }

    public void setThreadTimestamp(String ThreadTimestamp) {
        this.mThreadTimestamp = ThreadTimestamp;
    }
}
