package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class CommentItem {

    private String mCommentDescription, mCommentAuthor, mCommentTimestamp;

    public CommentItem(String CommentDescription, String CommentAuthor, String CommentTimestamp) {
        this.mCommentDescription = CommentDescription;
        this.mCommentAuthor = CommentAuthor;
        this.mCommentTimestamp = CommentTimestamp;
    }


    public String getCommentDescription() {
        return mCommentDescription;
    }

    public void setCommentDescription(String CommentDescription) {
        this.mCommentDescription = CommentDescription;
    }

    public String getCommentAuthor() {
        return mCommentAuthor;
    }

    public void setCommentAuthor(String CommentAuthor) {
        this.mCommentAuthor = CommentAuthor;
    }

    public String getCommentTimestamp() {
        return mCommentTimestamp;
    }

    public void setCommentTimestamp(String CommentTimestamp) {
        this.mCommentTimestamp = CommentTimestamp;
    }
}
