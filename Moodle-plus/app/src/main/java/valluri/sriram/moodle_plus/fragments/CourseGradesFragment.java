package valluri.sriram.moodle_plus.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.activities.CourseActivity;
import valluri.sriram.moodle_plus.adapters.CourseGradesListAdapter;
import valluri.sriram.moodle_plus.items.CourseGradeItem;
import valluri.sriram.moodle_plus.items.StringRequest;


/**
 * A simple {@link Fragment} subclass.
 */
public class CourseGradesFragment extends Fragment {
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArrayCourseGrades;
    private CourseGradesListAdapter CourseGradesListAdapter;
    private String[] CourseGradeAssignment, CourseGradeScore, CourseGradeWeightage, CourseGradeAbsoluteMarks;
    private ListView mListView;
    private View mView;
    private double score, weightage, out_of, absolute;
    private TextView mTextView;
    private String mCourseCode;

    public CourseGradesFragment() {
        // Required empty public constructor
    }
    public static CourseGradesFragment newInstance() {
        return new CourseGradesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        super.onActivityCreated(savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_course_grades, container, false);

        InitializeArrays();
        getCourseGrades();
        return mView;
    }
    
    /**
      * Method to initialize arrays
      */
    private void InitializeArrays() {
        CourseGradeAssignment = new String[0];
        CourseGradeScore = new String[0];
        CourseGradeWeightage = new String[0];
        CourseGradeAbsoluteMarks = new String[0];
    }
    
    /**
      * Method to raise dialog
      *
      */
    private void RaiseDialog(String Title,String Message) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(Title);
            alertDialog.setMessage(Message);
            alertDialog.setNegativeButton("close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OnClicking it closes the dialog
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }catch(Exception e) {
            Log.d("TAG", "Show Dialog: " + e.getMessage());
        }
    }
    
    /**
      * Method to get course grades of assignments/minors
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      *
      */
    private void getCourseGrades() {
        CourseActivity activity = (CourseActivity) getActivity();
        mCourseCode = activity.getIntent().getExtras().getString("CourseCode");
        String url = getString(R.string.IP_VALUE_PORT)+"/courses/course.json/"+mCourseCode+"/grades";
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArrayCourseGrades = responseJSON.getJSONArray("grades");
                    int k = jsonArrayCourseGrades.length();
                    CourseGradeAssignment = new String[k];
                    CourseGradeScore = new String[k];
                    CourseGradeWeightage = new String[k];
                    CourseGradeAbsoluteMarks = new String[k];
                    for (int i = 0; i < k ; i++) {
                        JSONObject jsonObjectCourseGrades = jsonArrayCourseGrades.getJSONObject(i);
                        score = jsonObjectCourseGrades.getDouble("score");
                        out_of = jsonObjectCourseGrades.getDouble("out_of");
                        weightage = jsonObjectCourseGrades.getDouble("weightage");
                        absolute = ((score / out_of) * (weightage));
                        absolute = Math.round(absolute * 100.0) / 100.0;
                        CourseGradeAssignment[i] = jsonObjectCourseGrades.getString("name");
                        CourseGradeScore[i] = String.valueOf(score)+"/"+String.valueOf(out_of);
                        CourseGradeWeightage[i] = String.valueOf(weightage);
                        CourseGradeAbsoluteMarks[i] = String.valueOf(absolute);
                    }
                    makeCourseGrades(mView);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get CourseGrades", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    /**
      * Method to make grades of course in View type
      *
      */
    private void makeCourseGrades(View view){
        //arrayList initialization for CourseGradeItem
        ArrayList<CourseGradeItem> CourseGradeItems = new ArrayList<CourseGradeItem>();

        for (int i = 0; i < CourseGradeAssignment.length; i++) {
            CourseGradeItem items = new CourseGradeItem(CourseGradeAssignment[i], CourseGradeScore[i], CourseGradeWeightage[i], CourseGradeAbsoluteMarks[i]);
            CourseGradeItems.add(items);
        }
         
        //Initializing courseGradesListAdapter
        CourseGradesListAdapter = new CourseGradesListAdapter(getActivity(), CourseGradeItems);
        mListView = (ListView) view.findViewById(R.id.lv_fragment_course_grades);
        mTextView = (TextView) view.findViewById(R.id.tv_course_grades_empty);
        mListView.setAdapter(CourseGradesListAdapter);
        if(CourseGradeAssignment.length == 0) {
            mTextView.setVisibility(View.VISIBLE);
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RaiseDialog(CourseGradeAssignment[position], "Score : " + CourseGradeScore[position] + "\n" + "Weightage : " + CourseGradeWeightage[position] + "\n" + "Absolute Marks : " + CourseGradeAbsoluteMarks[position]);
            }
        });
    }
}
