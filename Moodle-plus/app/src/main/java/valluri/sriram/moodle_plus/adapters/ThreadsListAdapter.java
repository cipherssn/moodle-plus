package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.ThreadItem;

public class ThreadsListAdapter extends BaseAdapter{

    Context context;
    List<ThreadItem> ThreadItem;
    private TextView mThreadTitle, mThreadDescription, mThreadAuthor, mThreadTimestamp;
    /*
     * Constructor method
     *
     */
    public ThreadsListAdapter(Context context, List<ThreadItem> ThreadItem) {
        this.context = context;
        this.ThreadItem = ThreadItem;
    }

    @Override
    public int getCount() {
        return ThreadItem.size();
    }

    @Override
    public Object getItem(int position) {
        return ThreadItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ThreadItem.indexOf(getItem(position));
    }

    @Override
    /**
      * Method to get View to get details of the particular thread
      *
      */ 
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.fragment_threads_item, null);
        }

        mThreadTitle = (TextView) convertView.findViewById(R.id.tv_threads_title);
        mThreadDescription = (TextView) convertView.findViewById(R.id.tv_threads_description);
        mThreadAuthor = (TextView) convertView.findViewById(R.id.tv_threads_author);
        mThreadTimestamp = (TextView) convertView.findViewById(R.id.tv_threads_timestamp);

        ThreadItem Threaditem = ThreadItem.get(position);
        // setting the image resource and title
        mThreadTitle.setText(Threaditem.getThreadTitle());
        mThreadDescription.setText(Threaditem.getThreadDescription());
        mThreadAuthor.setText(Threaditem.getThreadAuthor());

        String startTime = Threaditem.getThreadTimestamp();
        StringTokenizer tk = new StringTokenizer(startTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateParsed = null;
        Date dt = null;
        try {
            dateParsed = (Date)formatter.parse(date);
            dt = sdf.parse(time);
            System.out.println("Time Display: " + dateParsed+sdfs.format(dt));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mThreadTimestamp.setText(dateParsed.toString().substring(0,10) +" "+sdfs.format(dt));
        return convertView;
    }

}
