package valluri.sriram.moodle_plus.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.activities.CourseActivity;
import valluri.sriram.moodle_plus.activities.HomeActivity;
import valluri.sriram.moodle_plus.adapters.NotificationsListAdapter;
import valluri.sriram.moodle_plus.items.NotificationItem;
import valluri.sriram.moodle_plus.items.StringRequest;

//import com.android.volley.toolbox.StringRequest;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class NotificationsFragment extends Fragment {

    private String[] notificationCourseNameArray, notificationAuthorArray, notificationTimeStampArray;
    private NotificationsListAdapter notificationsListAdapter;
    private ListView mListView;
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArray;
    private View mView;
    private TextView mTextView;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_notifications, container, false);
        InitializeArrays();
        getNotifications();
        return mView;
    }

    /**
      * Initialize arrays
      *
      */
    private void InitializeArrays() {
        notificationCourseNameArray = new String[0];
        notificationAuthorArray = new String[0];
        notificationTimeStampArray = new String[0];
    }
    
    /**
      * Method to get notifications
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object to get details
      *
      */
    private void getNotifications() {
        String url=getString(R.string.IP_VALUE_PORT)+"/default/notifications.json";
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArray = responseJSON.getJSONArray("notifications");
                    notificationCourseNameArray = new String[jsonArray.length()];
                    notificationAuthorArray = new String[jsonArray.length()];
                    notificationTimeStampArray = new String[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        notificationTimeStampArray[i] = jsonObject.getString("created_at");
                        notificationAuthorArray[i] = "Posted by " + StringAbstract(jsonObject.getString("description"), 0);
                        notificationCourseNameArray[i] = StringAbstract(jsonObject.getString("description"), 4).toUpperCase();
                    }
                    MakeNotification(mView);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get notifications", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }
     
    /**
      * Method to make notification details in type view 
      * 
      */
    private void MakeNotification(View view) {
        ArrayList<NotificationItem> notificationItems = new ArrayList<NotificationItem>();

        for (int i = 0; i < notificationCourseNameArray.length; i++) {
            NotificationItem items = new NotificationItem(notificationCourseNameArray[i], notificationAuthorArray[i], notificationTimeStampArray[i]);
            notificationItems.add(items);
        }
        
        // update notificationsListAdapter with notification items
        notificationsListAdapter = new NotificationsListAdapter(getActivity(), notificationItems);
        mListView = (ListView) view.findViewById(R.id.lv_fragment_notifications);
        mTextView = (TextView) view.findViewById(R.id.tv_notification_empty);
        mListView.setAdapter(notificationsListAdapter);
        if(notificationCourseNameArray.length == 0){
            mTextView.setVisibility(View.VISIBLE);
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), CourseActivity.class);
                intent.putExtra("CourseName",notificationCourseNameArray[position]);
                intent.putExtra("CourseCode",notificationCourseNameArray[position]);
                startActivity(intent);
            }
        });
    }

    /**
      * Method to abstract string between >,< 
      *
      */
    private String StringAbstract(String Description ,int position) {
        String a = "";
        for(int i =0 ; i<Description.length(); i++) {
            if(Description.substring(i,i+1).equals(">")) {
                int j=i+1;
                while((j<Description.length())&&(!Description.substring(j,j+1).equals("<"))) {
                    a = a + Description.substring(j,j+1);
                    j++;
                }
                i=j;
                a=a+"-";
            }
        }
        String[] x = a.split("-");
        return x[position];
    }

}
