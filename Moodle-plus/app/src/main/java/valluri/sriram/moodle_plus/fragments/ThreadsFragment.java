package valluri.sriram.moodle_plus.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.activities.CommentActivity;
import valluri.sriram.moodle_plus.activities.CourseActivity;
import valluri.sriram.moodle_plus.activities.ThreadActivity;
import valluri.sriram.moodle_plus.adapters.ThreadsListAdapter;
import valluri.sriram.moodle_plus.items.StringRequest;
import valluri.sriram.moodle_plus.items.ThreadItem;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThreadsFragment extends Fragment {

    private ThreadsListAdapter threadsListAdapter;
    private String[] threadTitleArray, threadDescArray, threadAuthorArray, threadTimestampArray;
    private int[] threadId;
    private ListView mListView;
    private View mView;
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private JSONArray jsonArray;
    private TextView mTextView;
    private String mCourseCode;

    public static ThreadsFragment newInstance() {
        return new ThreadsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_threads, container, false);
        FloatingActionButton fab = (FloatingActionButton)mView.findViewById(R.id.fab_fragment_thread);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ThreadActivity.class);
                intent.putExtra("CourseCode", mCourseCode);
                startActivity(intent);
            }
        });
        InitializeArrays();
        getThreads();
        return mView;
    }

    /**
      * Initialize arrays
      */
    private void InitializeArrays() {
        threadTitleArray = new String[0];
        threadDescArray = new String[0];
        threadAuthorArray = new String[0];
        threadTimestampArray = new String[0];
        threadId = new int[0];
    }

    /**
      * Method to get Threads of corresponding courses
      * This method will call the server using volley and
      * onResponse takes the string and converts it into JSON object to get details
      */
    private void getThreads() {
        String url = getString(R.string.IP_VALUE_PORT)+"//courses/course.json/";
        CourseActivity activity = (CourseActivity) getActivity();
        mCourseCode = activity.getIntent().getExtras().getString("CourseCode");
        url = url + mCourseCode + "/threads";
        Log.i("Server Connecting", url);

        requestQueue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this.getActivity(), Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    jsonArray = responseJSON.getJSONArray("course_threads");
                    int k = jsonArray.length();
                    threadTitleArray = new String[k];
                    threadDescArray = new String[k];
                    threadAuthorArray = new String[k];
                    threadTimestampArray = new String[k];
                    threadId = new int[k];
                    for (int i = 0; i < k ; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        threadTitleArray[k-i-1] = jsonObject.getString("title");
                        threadDescArray[k-i-1] = jsonObject.getString("description");
                        threadAuthorArray[k-i-1] = jsonObject.getString("title");
                        threadTimestampArray[k-i-1] = jsonObject.getString("created_at");
                        threadId[k-i-1] = jsonObject.getInt("id");
                    }
                    makeThreads(mView);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Fail to get Threads", Toast.LENGTH_LONG).show();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }

    /**
      * Method to make Threads details in type view
      *
      */
    private void makeThreads(View view) {
    	// ArrayList of ThreadItem
        ArrayList<ThreadItem> ThreadItems = new ArrayList<ThreadItem>();

        for (int i = 0; i < threadTitleArray.length; i++) {
            ThreadItem items = new ThreadItem(threadTitleArray[i], threadDescArray[i], "Posted by "+ threadAuthorArray[i], threadTimestampArray[i]);
            ThreadItems.add(items);
        }

        threadsListAdapter = new ThreadsListAdapter(getActivity(), ThreadItems);
        mListView = (ListView) view.findViewById(R.id.lv_fragment_threads);
        mTextView = (TextView) view.findViewById(R.id.tv_threads_empty);
        mListView.setAdapter(threadsListAdapter);
        mListView.setEmptyView(mTextView);
        if(threadTitleArray.length == 0){
            mTextView.setVisibility(View.VISIBLE);
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), CommentActivity.class);
                i.putExtra("ThreadTitle", threadTitleArray[position]);
                i.putExtra("ThreadNumber", String.valueOf(threadId[position]));
                startActivity(i);
            }
        });
    }

}
