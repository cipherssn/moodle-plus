package valluri.sriram.moodle_plus.items;

/**
 * Created by lens on 20/02/16.
 */
public class GradeItem {

    private String mGradeCourseName, mGradeAssignment, mGradeScore, mGradeWeightage, mGradeAbsoluteMarks;

    public GradeItem(String GradeCourseName, String GradeAssignment, String GradeScore, String GradeWeightage, String GradeAbsoluteMarks) {
        this.mGradeCourseName = GradeCourseName;
        this.mGradeAssignment = GradeAssignment;
        this.mGradeScore = GradeScore;
        this.mGradeWeightage = GradeWeightage;
        this.mGradeAbsoluteMarks = GradeAbsoluteMarks;
        
    }

    public String getGradeCourseName() {
        return mGradeCourseName;
    }

    public void setGradeCourseName(String GradeCourseName) {
        this.mGradeCourseName = GradeCourseName;
    }

    public String getGradeAssignment() {
        return mGradeAssignment;
    }

    public void setGradeAssignment(String GradeAssignment) {
        this.mGradeAssignment = GradeAssignment;
    }

    public String getGradeScore() {
        return mGradeScore;
    }

    public void setGradeScore(String GradeScore) {
        this.mGradeScore = GradeScore;
    }

    public String getGradeWeightage() {
        return mGradeWeightage;
    }

    public void setGradeWeightage(String GradeWeightage) {
        this.mGradeWeightage = GradeWeightage;
    }

    public String getGradeAbsoluteMarks() {
        return mGradeAbsoluteMarks;
    }

    public void setGradeAbsoluteMarks(String GradeAbsoluteMarks) {
        this.mGradeAbsoluteMarks = GradeAbsoluteMarks;
    }
}
