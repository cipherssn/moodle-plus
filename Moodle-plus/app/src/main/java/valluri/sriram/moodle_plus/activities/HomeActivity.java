package valluri.sriram.moodle_plus.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.adapters.TabsPageAdapter;
import valluri.sriram.moodle_plus.items.StringRequest;

public class HomeActivity extends AppCompatActivity {

    private static final String POSITION = "POSITION";
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private RequestQueue requestQueue;
    private JSONObject responseJSON;
    private Boolean exit = false;
    private Boolean mLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setupToolbar();
        setupTabsWithPager();
    }
    @Override
    public void onSaveInstanceState(Bundle bundle) {    
        super.onSaveInstanceState(bundle);
        bundle.putInt(POSITION, mTabLayout.getSelectedTabPosition());
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mViewPager.setCurrentItem(savedInstanceState.getInt(POSITION));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_activity, menu);
        return true;
    }
    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }

    }
    
    /**
      * Method to setup the toolbar 
      */
    private void setupToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_activity_home);
        // Checks if toolbar is null or not
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        //setting title
        getSupportActionBar().setTitle("Home");
    }

    /**
      * Method to setup the Tabs with Pager
      */
    private void setupTabsWithPager() {
        mTabLayout = (TabLayout) findViewById(R.id.tl_activity_home);
        mViewPager = (ViewPager) findViewById(R.id.pager_home);
        mViewPager.setAdapter(new TabsPageAdapter(getSupportFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
    }
    
    /**
      * This method will call the server using volley and 
      * onResponse takes the string and converts it into JSON object
      */
        public void MenuItemClick(MenuItem item) {
        String url = getString(R.string.IP_VALUE_PORT)+"/default/logout.json";
        Log.i("Server Connecting", url);

        final ProgressDialog processDialog = new ProgressDialog(this);
        processDialog.setMessage("Logging Out");
        processDialog.show();
        
        requestQueue = Volley.newRequestQueue(this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(this, Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    responseJSON = new JSONObject(response);
                    mLogout=true;
                    processDialog.hide();
                    LaunchLoginActivity(mLogout);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                processDialog.hide();
            }
        });
        // Add the request to the RequestQueue.
        requestQueue.add(stringRequest);
    }
    
    /**
      * Method to launch Login activity
      */
    private void LaunchLoginActivity(Boolean mLogout) {
        if(mLogout) {
            Intent intent = new Intent(this, LoginActivity.class);
            StringRequest.removeCookies(this);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(), "Could Not Logout", Toast.LENGTH_LONG).show();
        }
    }

}
