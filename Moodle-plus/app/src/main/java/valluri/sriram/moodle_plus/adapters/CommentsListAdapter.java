package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.CommentItem;

public class CommentsListAdapter extends BaseAdapter{

    Context context;
    List<CommentItem> CommentItem;
    private TextView mCommentDescription, mCommentAuthor, mCommentTimestamp;

    public CommentsListAdapter(Context context, List<CommentItem> CommentItem) {
        this.context = context;
        this.CommentItem = CommentItem;
    }

    @Override
    public int getCount() {
        return CommentItem.size();
    }

    @Override
    public Object getItem(int position) {
        return CommentItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return CommentItem.indexOf(getItem(position));
    }

    @Override
    /**
     * Method to get View which gives details of the comments
     *
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.activity_comment_item, null);
        }

        mCommentDescription = (TextView) convertView.findViewById(R.id.tv_comment_description);
        mCommentAuthor = (TextView) convertView.findViewById(R.id.tv_comment_author);
        mCommentTimestamp = (TextView) convertView.findViewById(R.id.tv_comment_timestamp);

        CommentItem Commentitem = CommentItem.get(position);
        // setting the image resource and title
        mCommentDescription.setText(Commentitem.getCommentDescription());
        mCommentAuthor.setText(Commentitem.getCommentAuthor());

        String startTime = Commentitem.getCommentTimestamp();
        StringTokenizer tk = new StringTokenizer(startTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateParsed = null;
        Date dt = null;
        try {
            dateParsed = (Date)formatter.parse(date);
            dt = sdf.parse(time);
            System.out.println("Time Display: " + dateParsed+sdfs.format(dt));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mCommentTimestamp.setText(dateParsed.toString().substring(0,10) +" "+sdfs.format(dt));
        return convertView;
    }

}
