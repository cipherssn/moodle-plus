package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.AssignmentItem;

public class AssignmentsListAdapter extends BaseAdapter{

    Context context;
    List<AssignmentItem> AssignmentItem;
    private TextView mAssignmentTitle, mAssignmentBody, mAssignmentTimeStamp;

    public AssignmentsListAdapter(Context context, List<AssignmentItem> AssignmentItem) {
        this.context = context;
        this.AssignmentItem = AssignmentItem;
    }

    @Override
    public int getCount() {
        return AssignmentItem.size();
    }

    @Override
    public Object getItem(int position) {
        return AssignmentItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return AssignmentItem.indexOf(getItem(position));
    }

    @Override
    /**
     * Method to get View which gives details of the particular assignment
     *
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.fragment_assignments_item, null);
        }

        mAssignmentTitle = (TextView) convertView.findViewById(R.id.tv_assignment_title);
        mAssignmentBody = (TextView) convertView.findViewById(R.id.tv_assignment_body);
        mAssignmentTimeStamp = (TextView) convertView.findViewById(R.id.tv_assignment_timestamp);

        AssignmentItem courseitem = AssignmentItem.get(position);

        String startTime = courseitem.getAssignmentTimeStamp();
        StringTokenizer tk = new StringTokenizer(startTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateParsed = null;
        Date dt = null;
        try {
            dateParsed = (Date)formatter.parse(date);
            dt = sdf.parse(time);
            System.out.println("Time Display: " + dateParsed+sdfs.format(dt));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // setting the image resource and title
        mAssignmentTitle.setText(courseitem.getAssignmentTitle());
        mAssignmentBody.setText(courseitem.getAssignmentBody());
        mAssignmentTimeStamp.setText(dateParsed.toString().substring(0,10) +" "+sdfs.format(dt));

        return convertView;

    }

}
