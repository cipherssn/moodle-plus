package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.CourseItem;

public class CoursesListAdapter extends BaseAdapter{

    Context context;
    List<CourseItem> CourseItem;
    private TextView mCourseName, mCourseDescription, mCourseCode, mCourseCredits;
    /**
     * Constructor Method
     *
     */
    public CoursesListAdapter(Context context, List<CourseItem> CourseItem) {
        this.context = context;
        this.CourseItem = CourseItem;
    }

    @Override
    public int getCount() {
        return CourseItem.size();
    }

    @Override
    public Object getItem(int position) {
        return CourseItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return CourseItem.indexOf(getItem(position));
    }

    @Override
    /**
     * Method to get View which gives details of the particular course
     *
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.fragment_courses_item, null);
        }

        mCourseName = (TextView) convertView.findViewById(R.id.tv_course_name);
        mCourseDescription = (TextView) convertView.findViewById(R.id.tv_course_description);
        mCourseCode = (TextView) convertView.findViewById(R.id.tv_course_code);
        mCourseCredits = (TextView) convertView.findViewById(R.id.tv_course_credits);

        CourseItem courseitem = CourseItem.get(position);
        // setting the image resource and title
        mCourseName.setText(courseitem.getCourseName());
        mCourseDescription.setText(courseitem.getCourseDescription());
        mCourseCode.setText(courseitem.getCourseCode());
        mCourseCredits.setText(courseitem.getCourseCredits());
        return convertView;
    }

}
