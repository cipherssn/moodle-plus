package valluri.sriram.moodle_plus.adapters;

/**
 * Created by lens on 20/02/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import valluri.sriram.moodle_plus.R;
import valluri.sriram.moodle_plus.items.GradeItem;

public class GradesListAdapter extends BaseAdapter{

    Context context;
    List<GradeItem> GradeItem;
    private TextView mGradeCourseName, mGradeAssignment, mGradeScore, mGradeWeightage, mGradeAbsoluteMarks;
    /**
      * Constructor method 
      *
      */ 
    public GradesListAdapter(Context context, List<GradeItem> GradeItem) {
        this.context = context;
        this.GradeItem = GradeItem;

    }

    @Override
    public int getCount() {
        return GradeItem.size();
    }

    @Override
    public Object getItem(int position) {
        return GradeItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return GradeItem.indexOf(getItem(position));
    }

    @Override
    /**
      * Method to get View which gives information about grades
      *
      */ 
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.fragment_grades_item, null);
        }

        mGradeCourseName = (TextView) convertView.findViewById(R.id.tv_grades_course_name);
        mGradeAssignment = (TextView) convertView.findViewById(R.id.tv_grades_assignment);
        mGradeScore = (TextView) convertView.findViewById(R.id.tv_grades_score);
        mGradeWeightage = (TextView) convertView.findViewById(R.id.tv_grades_weightage);
        mGradeAbsoluteMarks = (TextView) convertView.findViewById(R.id.tv_grades_absolute_marks);

        GradeItem Gradeitem = GradeItem.get(position);
        // setting the image resource and title
        mGradeCourseName.setText(Gradeitem.getGradeCourseName());
        mGradeAssignment.setText(Gradeitem.getGradeAssignment());
        mGradeScore.setText(Gradeitem.getGradeScore());
        mGradeWeightage.setText(Gradeitem.getGradeWeightage());
        mGradeAbsoluteMarks.setText(Gradeitem.getGradeAbsoluteMarks());
        return convertView;

    }

}
